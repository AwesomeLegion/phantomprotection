package com.evildeedz.phantomprotection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Phantom;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class EventScheduler extends BukkitRunnable{

	private static Main main;
	public EventScheduler(Main main)
	{
		EventScheduler.main = main;
		this.runTaskTimer(main, 0, 100);
	}
	
	@Override
	public void run()
	{
		for(Player player: Bukkit.getOnlinePlayers())
		{
			// if player has permission
			if(player.hasPermission("phantomprotection.use"))
			{
				// if configuration section exists
				if(main.getConfig().isConfigurationSection(player.getUniqueId().toString()))
				{
					// if enabled
					if(main.getConfig().getString(player.getUniqueId().toString()+".enabled").equals("true"))
					{
						// Loop entities in a 10 radius
						for(Entity e: player.getNearbyEntities(10, 10, 10))
						{
							// if entity is phantom
							if(e instanceof Phantom)
							{
								// if phantom is not nametagged
								if(((Phantom) e).getCustomName() == null)
								{
									// kill phantom
									((Phantom) e).damage(2000);
								}
							}
						}
					}
				}
			}
		}
	}
}
