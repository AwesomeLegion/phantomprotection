package com.evildeedz.phantomprotection;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class Commands implements CommandExecutor{

	private static Main main;
	
	public Commands(Main main)
	{
		Commands.main = main;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		// If sender is a player or not
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			// if command has no arguements
			if(args.length <1)
			{
				// if player has permission or not
				if(player.hasPermission("phantomprotection.use"))
				{
					// If config section of the player does not exist
					if(!main.getConfig().isConfigurationSection(player.getUniqueId().toString()))
					{
						main.getConfig().createSection(player.getUniqueId().toString());
						main.getConfig().createSection(player.getUniqueId().toString()+".enabled");
						main.getConfig().set(player.getUniqueId().toString()+".enabled", "true");
						player.sendMessage(ChatColor.GREEN + "Phantom Protection " + ChatColor.YELLOW + "Enabled" + ChatColor.GREEN + ".");
					}
					else
					{
						// If already enabled..else
						if(main.getConfig().getString(player.getUniqueId().toString()+".enabled").equals("true"))
						{
							main.getConfig().set(player.getUniqueId().toString()+".enabled", "false");
							player.sendMessage(ChatColor.GREEN + "Phantom Protection " + ChatColor.RED + "Disabled" + ChatColor.GREEN + ".");
						}
						else
						{
							main.getConfig().set(player.getUniqueId().toString()+".enabled", "true");
							player.sendMessage(ChatColor.GREEN + "Phantom Protection " + ChatColor.YELLOW + "Enabled" + ChatColor.GREEN + ".");
						}
					}
				}
				else
				{
					player.sendMessage(ChatColor.RED + "You do not have permission to use this command!");
				}
			}
			
		}
		else
		{
			System.out.println("Cannot execute command from console");
		}
		main.saveConfig();
		return false;
	}
}
