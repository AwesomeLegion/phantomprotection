package com.evildeedz.phantomprotection;

import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin{

	@Override
	public void onEnable()
	{
		// Config
		getConfig().options().copyDefaults();
		saveDefaultConfig();
		
		new EventScheduler(this);
		getCommand("phantomprotection").setExecutor(new Commands(this));
	}
}
